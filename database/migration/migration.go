package migration

import (
	"microservice/entity"

	"gitlab.com/afif0808/golib/db"
)

func AutoMigrate(orm db.ORM) {
	gormInst := orm.GetGormInstance()
	err := gormInst.AutoMigrate(
		&entity.User{},
	)
	if err != nil {
		panic(err)
	}
}
