package repository

import (
	"context"
	"microservice/shared/redis"
)

type (
	CacheRepository interface {
		Get(ctx context.Context, key string) (interface{}, error)
		Set(ctx context.Context, key string, value interface{}, duration int64) error
		Delete(ctx context.Context, key string) error
	}

	cacheRepository struct {
		redis.RedisClient
	}
)

func NewCacheRepository(rdcl redis.RedisClient) CacheRepository {
	return &cacheRepository{RedisClient: rdcl}
}
