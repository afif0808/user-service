package repository

import (
	"context"
	"microservice/entity"
	"net/http"

	"gitlab.com/afif0808/golib/db"
	"gitlab.com/afif0808/golib/errors"
	"gitlab.com/afif0808/golib/param"
	"gorm.io/gorm"
)

type (
	UserRepository interface {
		GetUser(ctx context.Context, pa param.Param) (entity.User, error)
		GetUserList(ctx context.Context, pa param.ListParam) ([]entity.User, error)
		CountUser(ctx context.Context, pa param.CountParam) (int64, error)
		UpdateUser(ctx context.Context, usr entity.User, fields []string) error
	}

	userRepository struct {
		orm db.ORM
	}
)

func (r *userRepository) GetUser(ctx context.Context, pa param.Param) (entity.User, error) {
	var usr entity.User
	err := pa.ApplyToORM(r.orm).WithContext(ctx).First(&usr)
	if err == gorm.ErrRecordNotFound {
		err = errors.ErrBase.New("user with given id was not found").
			WithProperty(errors.ErrCodeProperty, http.StatusNotFound).
			WithProperty(errors.ErrHttpCodeProperty, http.StatusNotFound)
	}

	return usr, err
}

func (r *userRepository) GetUserList(ctx context.Context, pa param.ListParam) ([]entity.User, error) {
	var usrs []entity.User
	err := pa.ApplyToORM(r.orm).
		WithContext(ctx).
		Where("deleted_at IS NULL").
		Find(&usrs)
	return usrs, err
}

func (r *userRepository) CountUser(ctx context.Context, pa param.CountParam) (int64, error) {
	var count int64
	err := pa.ApplyToORM(r.orm).
		WithContext(ctx).
		Where("deleted_at IS NULL").
		Model(&entity.User{}).
		Count(&count)
	return count, err
}

func (r *userRepository) UpdateUser(ctx context.Context, usr entity.User, fields []string) error {
	if len(fields) <= 0 {
		return r.orm.Select("*").Update(&usr)
	}

	columns := make([]interface{}, len(fields))
	for i, v := range fields {
		columns[i] = v
	}

	return r.orm.Select(nil, columns...).Update(&usr)
}

func NewUserRepository(orm db.ORM) UserRepository {
	return &userRepository{orm: orm}
}
