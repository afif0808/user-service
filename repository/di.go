package repository

import "go.uber.org/dig"

type (
	Holder struct {
		dig.In
		UserRepository
		CacheRepository
	}
)

func Register(container *dig.Container) error {
	err := container.Provide(NewUserRepository)
	if err != nil {
		return err
	}

	err = container.Provide(NewCacheRepository)
	if err != nil {
		return err
	}

	return nil
}
