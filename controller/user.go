package controller

import (
	"microservice/service"
	"microservice/shared"
	"microservice/shared/dto"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/afif0808/golib/context"
	"gitlab.com/afif0808/golib/errors"
)

type (
	UserController struct {
		services service.Holder
	}
)

func (c *UserController) VerifyUser(ctx echo.Context) error {
	pctx := context.NewEmptyPrixaContext(ctx)
	var req dto.VerifyUserRequest
	err := ctx.Bind(&req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	err = ctx.Validate(req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	err = c.services.VerifyUser(ctx.Request().Context(), req)
	if err != nil {
		return err
	}

	return pctx.Success(nil)
}

func (c *UserController) ResendVerification(ctx echo.Context) error {
	pctx := context.NewEmptyPrixaContext(ctx)
	var req dto.ResendVerificationRequest
	err := ctx.Bind(&req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	err = ctx.Validate(req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	err = c.services.ResendUserVerificationEmail(ctx.Request().Context(), req)
	if err != nil {
		return err
	}

	return pctx.Success(nil)
}

func (c *UserController) GetUserList(ctx echo.Context) error {
	var (
		pctx = context.NewEmptyPrixaContext(ctx)
	)

	listReq, err := BindListParam(ctx)
	if err != nil {
		return errors.ErrBase.New(err.Error()).WithProperty(errors.ErrCodeProperty, http.StatusBadRequest).WithProperty(errors.ErrHttpCodeProperty, http.StatusBadRequest)
	}

	usrs, count, err := c.services.GetUserList(ctx.Request().Context(), listReq)
	if err != nil {
		return err
	}

	return pctx.SuccessWithMeta(usrs, newListInfo(listReq, count))
}

func (c *UserController) RefreshToken(ctx echo.Context) error {
	var (
		pctx = context.NewEmptyPrixaContext(ctx)
	)
	var req dto.UserRefreshTokenRequest
	err := ctx.Bind(&req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	err = ctx.Validate(req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	resp, err := c.services.UserService.RefreshToken(ctx.Request().Context(), req)
	if err != nil {
		return err
	}

	return pctx.Success(resp)
}

func (c *UserController) RequestPasswordReset(ctx echo.Context) error {
	pctx := context.NewEmptyPrixaContext(ctx)
	var req dto.PasswordResetRequestParam
	err := ctx.Bind(&req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	err = ctx.Validate(req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	err = c.services.UserService.RequestPasswordReset(ctx.Request().Context(), req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	return pctx.Success(nil)
}

func (c *UserController) ResetPassword(ctx echo.Context) error {
	pctx := context.NewEmptyPrixaContext(ctx)
	var req dto.ResetPasswordRequest
	err := ctx.Bind(&req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	err = ctx.Validate(req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	err = c.services.UserService.ResetUserPassword(ctx.Request().Context(), req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	return pctx.Success(nil)
}
func (c *UserController) GetUserByID(ctx echo.Context) error {
	pctx := context.NewEmptyPrixaContext(ctx)
	id := ctx.Param(":id")

	usr, err := c.services.GetUserByID(ctx.Request().Context(), id)
	if err != nil {
		return shared.BadRequestError(err)
	}

	return pctx.Success(usr)
}

func (c *UserController) UpdateUserPassword(ctx echo.Context) error {
	pctx := context.NewEmptyPrixaContext(ctx)
	var req dto.UpdateUserPasswordRequest
	err := ctx.Bind(&req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	err = ctx.Validate(req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	err = c.services.UpdateUserPassword(ctx.Request().Context(), req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	return pctx.Success(nil)
}

func (c *UserController) CreateRedirectToken(ctx echo.Context) error {
	pctx, _ := ctx.(*context.PrixaContext)
	var req dto.CreateRedirectTokenRequest
	if pctx.Session == nil || pctx.Session.ClientAppID == nil {
		return shared.BadRequestError(errors.ErrBase.New("empty session or is invalid"))
	}
	err := ctx.Bind(&req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	req.UserID = pctx.Session.UserId

	err = ctx.Validate(req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	if *pctx.Session.ClientAppID != req.ClientAppID {
		return shared.BadRequestError(errors.ErrBase.New("invalid client app"))
	}

	resp, err := c.services.CreateRedirectToken(ctx.Request().Context(), req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	return pctx.Success(resp)
}

func (c *UserController) ClaimRedirectToken(ctx echo.Context) error {
	pctx := context.NewEmptyPrixaContext(ctx)
	var req dto.ClaimRedirectTokenRequest
	err := ctx.Bind(&req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	err = ctx.Validate(req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	resp, err := c.services.ClaimRedirectToken(ctx.Request().Context(), req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	return pctx.Success(resp)
}

func (c *UserController) Login(ctx echo.Context) error {
	pctx := context.NewEmptyPrixaContext(ctx)
	var req dto.UserLoginRequest
	err := ctx.Bind(&req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	err = ctx.Validate(req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	resp, _, err := c.services.Login(ctx.Request().Context(), req)
	if err != nil {
		return shared.BadRequestError(err)
	}

	return pctx.Success(resp)
}

func NewUserController(services service.Holder) *UserController {
	return &UserController{services: services}
}
