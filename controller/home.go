package controller

import (
	"microservice/service"
	"microservice/shared/dto"

	"github.com/labstack/echo/v4"
	"gitlab.com/afif0808/golib/context"
	"gitlab.com/afif0808/golib/errors"
)

type (
	HomeController struct {
		services service.Holder
	}
)

func NewHomeController(services service.Holder) (*HomeController, error) {
	return &HomeController{services: services}, nil
}

func (c *HomeController) Check(ctx echo.Context) error {
	var (
		pctx = context.NewEmptyPrixaContext(ctx)
	)

	resp, err := c.services.HomeService.Check(pctx.Request().Context())
	if err != nil {
		return err
	}

	return pctx.Success(resp)
}

func (c *HomeController) Validate(ctx echo.Context) error {
	var (
		pctx, ok = ctx.(*context.PrixaContext)
	)

	if !ok {
		return errors.ErrSession
	}

	resp := dto.ValidateTokenResponse{Session: pctx.Session}
	return pctx.Success(resp)
}
