package controller

import (
	"math"
	"microservice/shared/dto"

	"gitlab.com/afif0808/golib/param"
)

func newListInfo(lp param.ListParam, recordCount int64) dto.ListInfo {
	pageCount := int64(math.Ceil(float64(recordCount) / float64(lp.Limit)))
	return dto.ListInfo{
		Page:        lp.Page,
		Limit:       lp.Limit,
		RecordCount: recordCount,
		PageCount:   pageCount,
		IsFirstPage: lp.Page == 1,
		IsLastPage:  lp.Page == pageCount,
	}
}
