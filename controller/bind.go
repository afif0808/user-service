package controller

import (
	"errors"
	"strings"

	"github.com/labstack/echo/v4"
	"gitlab.com/afif0808/golib/param"
)

// controller/bind.go
func BindListParam(ctx echo.Context) (param.ListParam, error) {
	listParam := param.ListParam{
		PaginationParam: param.PaginationParam{
			Page:  1,
			Limit: 10,
		},
		FilterKeyAliases: map[string]string{},
	}
	err := echo.QueryParamsBinder(ctx).
		Int64("page", &listParam.Page).
		Int64("limit", &listParam.Limit).
		String("sort_by", &listParam.SortBy).
		String("sort_type", &listParam.SortType).
		CustomFunc("filter", func(values []string) []error {
			var errs []error
			listParam.FilterParams = make([]param.FilterParam, len(values))
			for i, v := range values {
				keyValue := strings.Split(v, ":")
				if len(keyValue) < 2 {
					errs = append(errs, errors.New("invalid filter format"))
					break
				}
				listParam.FilterParams[i] = param.FilterParam{
					Key:   keyValue[0],
					Value: &keyValue[1],
				}
			}
			return errs
		}).BindError()

	return listParam, err

}
