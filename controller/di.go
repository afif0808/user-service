package controller

import (
	"microservice/shared"
	internalmiddleware "microservice/shared/middleware"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/afif0808/golib/context"
	"gitlab.com/afif0808/golib/echo/middlewares"
	"go.uber.org/dig"
)

type (
	Holder struct {
		dig.In

		Deps                shared.Deps
		SessionMiddleware   middlewares.SessionMiddleware
		InternalMiddlewares internalmiddleware.Middlewares
		HomeController      *HomeController
		UserController      *UserController
	}
)

func Register(container *dig.Container) error {
	if err := container.Provide(NewHomeController); err != nil {
		return err
	}
	if err := container.Provide(NewUserController); err != nil {
		return err
	}
	if err := internalmiddleware.Register(container); err != nil {
		return err
	}

	return nil
}

func (h *Holder) SetupRoutes(app *echo.Echo) {

	app.Validator = h.Deps.CustomValidator
	app.HTTPErrorHandler = h.ErrorHandler

	app.Use(middleware.Recover())

	v1 := app.Group("/v1")
	{
		test := v1.Group("/check")
		test.GET("", h.HomeController.Check)
		test.GET("/validate", h.HomeController.Validate, h.SessionMiddleware.AuthenticateSession)
	}

	userRoutes := v1.Group("/users/")
	{
		userRoutes.POST("login", h.UserController.Login)
	}

}

func (h *Holder) ErrorHandler(err error, c echo.Context) {
	var (
		sctx, ok = c.(*context.PrixaContext)
	)

	if !ok {
		sctx = context.NewEmptyPrixaContext(c)
	}

	if httpError, ok := err.(*echo.HTTPError); ok && httpError.Code == http.StatusNotFound {
		c.JSON(http.StatusNotFound, context.Failed{Message: "not found", Code: http.StatusNotFound})
		return
	}

	h.Deps.Logger.Errorf(
		"path=%s error=%s",
		sctx.Path(),
		err,
	)

	_ = sctx.Fail(err)
}
