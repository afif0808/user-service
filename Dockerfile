FROM golang:alpine as builder
ENV REPODIR gitlab.com/afif0808/user-service
COPY . ${REPODIR}
WORKDIR ${REPODIR}
RUN go build -o app cmd/service/main.go

FROM alpine:latest
ENV REPODIR gitlab.com/afif0808/user-service
WORKDIR /root/
COPY --from=builder ${REPODIR}/app /root/.
CMD [ "./app" ]