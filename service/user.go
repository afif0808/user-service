package service

import (
	"context"
	"errors"
	"fmt"
	"microservice/entity"
	"microservice/repository"
	"microservice/shared"
	"microservice/shared/auth"
	"microservice/shared/config"
	"microservice/shared/dto"
	"microservice/shared/encryptions"
	_mailer "microservice/shared/mailer"
	"microservice/shared/mapper"

	gonanoid "github.com/matoous/go-nanoid/v2"
	"gitlab.com/afif0808/golib/crypto"
	"gitlab.com/afif0808/golib/param"
	"gitlab.com/afif0808/golib/session"
	"golang.org/x/crypto/bcrypt"
)

type (
	UserService interface {
		Login(ctx context.Context, req dto.UserLoginRequest) (dto.UserLoginResponse, entity.User, error)
		SendUserVerificationEmail(ctx context.Context, usr entity.User) error
		ResendUserVerificationEmail(ctx context.Context, req dto.ResendVerificationRequest) error
		VerifyUser(ctx context.Context, req dto.VerifyUserRequest) error
		GetUserList(ctx context.Context, req param.ListParam) ([]dto.UserResponse, int64, error)
		RefreshToken(ctx context.Context, req dto.UserRefreshTokenRequest) (dto.UserRefreshTokenResponse, error)
		RequestPasswordReset(ctx context.Context, param dto.PasswordResetRequestParam) error
		ResetUserPassword(ctx context.Context, req dto.ResetPasswordRequest) error
		UpdateUserPassword(ctx context.Context, req dto.UpdateUserPasswordRequest) error
		GetUserByID(ctx context.Context, userID string) (dto.UserResponse, error)
		CreateRedirectToken(ctx context.Context, req dto.CreateRedirectTokenRequest) (dto.CreateRedirectTokenResponse, error)
		ClaimRedirectToken(ctx context.Context, req dto.ClaimRedirectTokenRequest) (dto.AuthTokenPayload, error)
	}

	userServiceImpl struct {
		repo                  repository.Holder
		config                *config.Configuration
		userVerificationCrypt crypto.Crypto
		sessionCrypt          crypto.Crypto
		refreshTokenCrypt     crypto.Crypto
		mailer                _mailer.Mailer
	}
)

var (
	verificationTokenFormat = func(s string) string {
		return fmt.Sprintf("user_verification_token_%s", s)
	}
	refreshTokenFormat = func(s string) string {
		return fmt.Sprintf("refresh_token_%s", s)
	}
	resetPasswordTokenFormat = func(s string) string {
		return fmt.Sprintf("password_reset_token_%s", s)
	}
	redirectTokenFormat = func(s string) string {
		return fmt.Sprintf("redirect_token_%s", s)
	}
)

func (i *userServiceImpl) Login(ctx context.Context, req dto.UserLoginRequest) (dto.UserLoginResponse, entity.User, error) {
	rolePrefix := req.RolePrefix + "*"
	pa := param.Param{FilterParams: []param.FilterParam{
		{Key: "email", Value: &req.Email},
		{Key: "role", Value: &rolePrefix},
		{Key: "deleted_at", Value: nil},
	}}

	usr, err := i.repo.GetUser(ctx, pa)
	if err != nil {
		return dto.UserLoginResponse{}, entity.User{}, shared.UnauthorizedError(err)
	}

	if usr.Status != "active" {
		return dto.UserLoginResponse{}, entity.User{}, shared.UnauthorizedError(errors.New("user is not yet activated"))
	}

	err = bcrypt.CompareHashAndPassword([]byte(usr.Password), []byte(req.Password))
	if err != nil {
		return dto.UserLoginResponse{}, entity.User{}, shared.UnauthorizedError(errors.New("password is not matched"))
	}

	var resp dto.UserLoginResponse
	ss := mapper.MapSession(usr)
	resp.AuthTokenPayload, err = auth.GenerateAuthToken(ss, i.config)
	if err != nil {
		return dto.UserLoginResponse{}, entity.User{}, err
	}

	err = i.repo.CacheRepository.Set(ctx, refreshTokenFormat(resp.AuthTokenPayload.RefreshToken), usr.ID, i.config.TTLRefresh)

	return resp, usr, err
}

func (i *userServiceImpl) VerifyUser(ctx context.Context, req dto.VerifyUserRequest) error {
	var userID string
	tokenKey := verificationTokenFormat(req.Token)
	if v, err := i.repo.CacheRepository.Get(ctx, tokenKey); err != nil {
		return shared.UnauthorizedError(err)
	} else {
		userID, _ = v.(string)
	}

	usr, err := i.getUserByID(ctx, userID)
	if err != nil {
		return shared.UnauthorizedError(err)
	}

	if req.NewPassword != nil {
		usr.Password, err = encryptions.EncryptPassword(*req.NewPassword)
		if err != nil {
			return err
		}
	}
	usr.Status = "active"
	err = i.repo.UpdateUser(ctx, usr, []string{"Status", "Password"})
	if err != nil {
		return err
	}
	return i.repo.CacheRepository.Delete(ctx, tokenKey)
}

func (i userServiceImpl) SendUserVerificationEmail(ctx context.Context, usr entity.User) error {
	token, err := gonanoid.New()
	if err != nil {
		return err
	}

	err = i.repo.CacheRepository.Set(ctx, verificationTokenFormat(token), usr.ID, 420)
	if err != nil {
		return err
	}

	// go i.mailer.Send(mail)

	// TODO: publish queue to mailer service
	mail := _mailer.MailTemplate{
		Subject:  "Prixa Account Registration",
		To:       usr.Email,
		Template: "d-c5feab6cb634467ba6b70f0cac8b5953",
		TemplateArgs: map[string]interface{}{
			"Sender_Name": usr.Name,
			"url":         "example.com/" + token,
		},
	}

	go i.mailer.SendWithTemplate(mail)

	return nil
}

func (i *userServiceImpl) ResendUserVerificationEmail(ctx context.Context, req dto.ResendVerificationRequest) error {
	rolePrefix := req.RolePrefix + "*"
	pa := param.Param{FilterParams: []param.FilterParam{
		{Key: "email", Value: &req.Email},
		{Key: "role", Value: &rolePrefix},
	}}
	usr, err := i.repo.GetUser(ctx, pa)
	if err != nil {
		return err
	}
	if usr.Status == "active" {
		return shared.UnauthorizedError(errors.New("user already verified"))
	}

	token, err := gonanoid.New()
	if err != nil {
		return err
	}

	err = i.repo.CacheRepository.Set(ctx, verificationTokenFormat(token), usr.ID, 420)
	if err != nil {
		return err
	}

	if err != nil {
		return err
	}

	mail := _mailer.MailTemplate{
		Subject:  "Prixa Account Registration",
		To:       usr.Email,
		Template: "d-c5feab6cb634467ba6b70f0cac8b5953",
		TemplateArgs: map[string]interface{}{
			"Sender_Name": usr.Name,
			"url":         "example.com/" + token,
		},
	}

	return i.mailer.SendWithTemplate(mail)
}

func (i userServiceImpl) GetUserList(ctx context.Context, pa param.ListParam) ([]dto.UserResponse, int64, error) {
	usrs, err := i.repo.GetUserList(ctx, pa)
	if err != nil {
		return nil, 0, err
	}

	count, err := i.repo.CountUser(ctx, pa.CountParam())
	if err != nil {
		return nil, 0, err
	}

	return mapper.MapUserResponses(usrs), count, nil
}

func (i userServiceImpl) RefreshToken(ctx context.Context, req dto.UserRefreshTokenRequest) (dto.UserRefreshTokenResponse, error) {
	ss, err := session.NewSession(i.refreshTokenCrypt, req.RefreshToken)
	if err != nil {
		return dto.UserRefreshTokenResponse{}, err
	}
	tokenKey := refreshTokenFormat(req.RefreshToken)
	userID, err := i.repo.CacheRepository.Get(ctx, tokenKey)
	if err != nil {
		return dto.UserRefreshTokenResponse{}, err
	}
	if ss.UserId != userID {
		return dto.UserRefreshTokenResponse{}, errors.New("invalid refresh token")
	}

	var resp dto.UserRefreshTokenResponse
	resp.AuthTokenPayload, err = auth.GenerateAuthToken(*ss, i.config)
	if err != nil {
		return dto.UserRefreshTokenResponse{}, err
	}
	newTokenKey := refreshTokenFormat(resp.AuthTokenPayload.RefreshToken)

	err = i.repo.CacheRepository.Set(ctx, newTokenKey, ss.UserId, i.config.TTLRefresh)
	if err != nil {
		return dto.UserRefreshTokenResponse{}, err
	}

	err = i.repo.Delete(ctx, tokenKey)
	// verify refresh token
	//
	return resp, err
}

func (i userServiceImpl) RequestPasswordReset(ctx context.Context, reqParam dto.PasswordResetRequestParam) error {
	rolePrefix := reqParam.RolePrefix + "*"
	pa := param.Param{FilterParams: []param.FilterParam{
		{Key: "email", Value: &reqParam.Email},
		{Key: "role", Value: &rolePrefix},
	}}

	usr, err := i.repo.GetUser(ctx, pa)
	if err != nil {
		return err
	}
	token, err := gonanoid.New()
	if err != nil {
		return err
	}

	err = i.repo.CacheRepository.Set(ctx, resetPasswordTokenFormat(token), usr.ID, 420)
	if err != nil {
		return err
	}

	mail := _mailer.MailTemplate{
		Subject:  "Prixa Account Password Reset",
		To:       usr.Email,
		Template: "d-cd218f1aa32f4c9fa0743cce27b26899",
		TemplateArgs: map[string]interface{}{
			"Sender_Name": usr.Name,
			"url":         "example.com",
		},
	}

	return i.mailer.SendWithTemplate(mail)
}

func (i userServiceImpl) ResetUserPassword(ctx context.Context, req dto.ResetPasswordRequest) error {
	var userID string
	tokenKey := resetPasswordTokenFormat(req.Token)
	if v, err := i.repo.CacheRepository.Get(ctx, tokenKey); err != nil {
		return err
	} else {
		userID, _ = v.(string)
	}

	if userID == "" {
		return shared.UnauthorizedError(errors.New("the token is either invalid or expired"))
	}

	usr, err := i.getUserByID(ctx, userID)
	if err != nil {
		return err
	}

	usr.Password, err = encryptions.EncryptPassword(req.NewPassword)
	if err != nil {
		return err
	}
	err = i.repo.UpdateUser(ctx, usr, []string{"Password"})
	if err != nil {
		return err
	}
	return i.repo.Delete(ctx, tokenKey)
}

func (i *userServiceImpl) GetUserByID(ctx context.Context, id string) (dto.UserResponse, error) {
	usr, err := i.getUserByID(ctx, id)
	if err != nil {
		return dto.UserResponse{}, err
	}
	return mapper.MapUserResponse(usr), nil
}

func (i *userServiceImpl) UpdateUserPassword(ctx context.Context, req dto.UpdateUserPasswordRequest) error {
	usr, err := i.getUserByID(ctx, req.ID)
	if err != nil {
		return shared.NotFoundError(err)
	}
	err = bcrypt.CompareHashAndPassword([]byte(usr.Password), []byte(req.CurrentPassword))
	if err != nil {
		return err
	}

	usr.Password, err = encryptions.EncryptPassword(req.NewPassword)
	if err != nil {
		return err
	}

	return i.repo.UpdateUser(ctx, usr, []string{"Password"})
}

func (i *userServiceImpl) CreateRedirectToken(ctx context.Context, req dto.CreateRedirectTokenRequest) (dto.CreateRedirectTokenResponse, error) {
	token, err := gonanoid.New()
	if err != nil {
		return dto.CreateRedirectTokenResponse{}, err
	}

	err = i.repo.CacheRepository.Set(ctx, redirectTokenFormat(token), req.UserID, i.config.TTLRedirectToken)
	if err != nil {
		return dto.CreateRedirectTokenResponse{}, err
	}

	return dto.CreateRedirectTokenResponse{Token: token}, nil
}

func (i *userServiceImpl) ClaimRedirectToken(ctx context.Context, req dto.ClaimRedirectTokenRequest) (dto.AuthTokenPayload, error) {
	var userID string
	tokenKey := redirectTokenFormat(req.Token)
	if v, err := i.repo.CacheRepository.Get(ctx, tokenKey); err != nil {
		return dto.AuthTokenPayload{}, err
	} else {
		userID = v.(string)
	}

	usr, err := i.getUserByID(ctx, userID)
	if err != nil {
		return dto.AuthTokenPayload{}, err
	}

	resp, err := auth.GenerateAuthToken(mapper.MapSession(usr), i.config)
	if err != nil {
		return dto.AuthTokenPayload{}, err
	}

	err = i.repo.CacheRepository.Delete(ctx, tokenKey)

	return resp, err
}

func NewUserService(repo repository.Holder, config *config.Configuration, mailer _mailer.Mailer) (UserService, error) {
	userVerificationCrypt, err := crypto.New(config.UserVerificationSecret)
	if err != nil {
		return nil, err
	}
	sessionCrypt, err := crypto.New(config.SessionSecret)
	if err != nil {
		return nil, err
	}
	refreshTokenCrypt, err := crypto.New(config.RefreshSecret)
	if err != nil {
		return nil, err
	}

	return &userServiceImpl{
		repo:                  repo,
		config:                config,
		userVerificationCrypt: userVerificationCrypt,
		sessionCrypt:          sessionCrypt,
		mailer:                mailer,
		refreshTokenCrypt:     refreshTokenCrypt,
	}, nil
}

func (i *userServiceImpl) getUserByID(ctx context.Context, id string) (entity.User, error) {
	usr, err := i.repo.UserRepository.GetUser(ctx,
		param.Param{FilterParams: []param.FilterParam{
			{Key: "id", Value: &id},
		}})
	return usr, err
}
