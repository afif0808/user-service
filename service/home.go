package service

import (
	"context"
	"microservice/repository"
	"microservice/shared"
	"microservice/shared/dto"
)

type (
	HomeService interface {
		Check(ctx context.Context) (*dto.HomeResponse, error)
	}

	impl struct {
		repo repository.Holder
		deps shared.Deps
	}
)

func (i *impl) Check(ctx context.Context) (*dto.HomeResponse, error) {
	return &dto.HomeResponse{Message: "ok"}, nil
}

func NewHomeService(repo repository.Holder, deps shared.Deps) (HomeService, error) {
	return &impl{repo, deps}, nil
}
