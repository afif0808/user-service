package service

import "go.uber.org/dig"

type (
	Holder struct {
		dig.In
		HomeService HomeService
		UserService
	}
)

func Register(container *dig.Container) error {
	if err := container.Provide(NewHomeService); err != nil {
		return err
	}
	if err := container.Provide(NewUserService); err != nil {
		return err
	}
	return nil
}
