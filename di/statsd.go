package di

import (
	"fmt"
	"microservice/shared/config"

	"gitlab.com/afif0808/golib/telemetry/prixastats"
)

const (
	defaultStatsAddress = "localhost:8125"
	appName             = "ganymede"
	envTag              = "env:%s"
)

func NewStatsD(cfg *config.Configuration) *prixastats.Client {
	address := cfg.DatadogHost
	if address == "" {
		address = defaultStatsAddress
	}

	return prixastats.New(prixastats.Address(address), prixastats.Namespace(appName), prixastats.Tags(fmt.Sprintf(envTag, cfg.DatadogEnv)))
}
