package di

import (
	"microservice/shared/config"

	"gitlab.com/afif0808/golib/crypto"
)

func NewCrypto(config *config.Configuration) (crypto.Crypto, error) {
	return crypto.New(config.SessionSecret)
}
