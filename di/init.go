package di

import (
	"microservice/controller"
	"microservice/repository"
	"microservice/service"
	"microservice/shared"
	"microservice/shared/config"
	"microservice/shared/mailer"
	"microservice/shared/redis"
	"microservice/shared/voximplant"

	"github.com/pkg/errors"
	"go.uber.org/dig"
)

var (
	Container *dig.Container = dig.New()
)

func init() {
	if err := Container.Provide(config.New); err != nil {
		panic(err)
	}

	if err := Container.Provide(NewOrm); err != nil {
		panic(err)
	}

	if err := Container.Provide(NewODM); err != nil {
		panic(err)
	}

	if err := Container.Provide(NewLogger); err != nil {
		panic(err)
	}

	if err := Container.Provide(NewCrypto); err != nil {
		panic(err)
	}

	if err := Container.Provide(NewPrixaSessionMiddleware); err != nil {
		panic(err)
	}

	if err := controller.Register(Container); err != nil {
		panic(err)
	}

	if err := service.Register(Container); err != nil {
		panic(err)
	}

	if err := repository.Register(Container); err != nil {
		panic(err)
	}

	if err := shared.Register(Container); err != nil {
		panic(err)
	}
	if err := redis.Register(Container); err != nil {
		panic(err)
	}
	if err := mailer.Register(Container); err != nil {
		panic(err)
	}

	if err := voximplant.Register(Container); err != nil {
		panic(err)
	}

	if err := Container.Provide(NewStatsD); err != nil {
		panic(errors.Wrap(err, "Failed to provide statsd"))
	}

}
