package di

import (
	"fmt"
	"microservice/shared/config"
	"time"

	"gitlab.com/afif0808/golib/db"
	"gitlab.com/afif0808/golib/logger"
)

func NewOrm(config *config.Configuration, logger logger.Logger) (db.ORM, error) {
	duration, err := time.ParseDuration(config.DbMaxLifeTimeConnection)
	DbConnectionString := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", config.DbUser, config.DbPass, config.DbHost, config.DbPort, config.DbName)
	if err != nil {
		return nil, err
	}

	var (
		opts = &db.MySqlOption{
			ConnectionString:      DbConnectionString,
			MaxOpenConnection:     config.DbMaxOpenConnection,
			MaxIdleConnection:     config.DbMaxIdleConnection,
			MaxLifeTimeConnection: duration,
		}
	)

	if config.EnableLogSqlQuery {
		opts.Logger = logger
	}

	return db.NewMySql(opts)
}

func NewODM(config *config.Configuration, logger logger.Logger) (db.ODM, error) {
	connectionTimeout, err := time.ParseDuration(config.ArangoConnectionTimeout)
	if err != nil {
		return nil, err
	}

	var (
		opts = &db.ArangoOption{
			DatabaseName:          config.ArangoDatabaseName,
			ConnectionString:      config.ArangoEndpoints,
			MaxLifeTimeConnection: connectionTimeout,
			MaxOpenConnection:     config.ArangoMaxConnection,
			Logger:                logger,
			Username:              config.ArangoUserName,
			Password:              config.ArangoUserPass,
		}
	)

	return db.NewArango(opts)
}
