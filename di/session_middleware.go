package di

import (
	"microservice/shared/config"

	"gitlab.com/afif0808/golib/crypto"
	"gitlab.com/afif0808/golib/echo/middlewares"
	"gitlab.com/afif0808/golib/logger"
)

func NewPrixaSessionMiddleware(config *config.Configuration, crypto crypto.Crypto, logger logger.Logger) (middlewares.SessionMiddleware, error) {
	return middlewares.NewSessionMiddleware(config.SessionSecret, crypto, logger)
}
