package entity

import "time"

type User struct {
	ID        string `gorm:"primaryKey;autoIncrement:false"`
	Email     string `gorm:"index:idx_unique,unique;size:100"`
	Password  string
	Name      string `gorm:"index:idx_name;size:50"`
	Role      string `gorm:"index:idx_role;size:50"`
	Status    string
	BelongsTo string `gorm:"index:idx_unique,unique;size:50"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}
