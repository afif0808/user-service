package mailer

import (
	"errors"
	"microservice/shared/config"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

type (
	sendgridImpl struct {
		client *sendgrid.Client
		sender string
	}
)

func (i *sendgridImpl) Send(ml Mail) error {
	from := mail.NewEmail("", i.sender)
	to := mail.NewEmail("", ml.To)

	resp, err := i.client.Send(mail.NewSingleEmail(from, ml.Subject, to, "", ml.Body))
	if err != nil {
		return err
	}
	if resp.StatusCode >= 400 {
		err = errors.New(resp.Body)
	}

	return err
}

func (i *sendgridImpl) SendWithTemplate(mlt MailTemplate) error {
	from := mail.NewEmail("", i.sender)
	to := mail.NewEmail("", mlt.To)

	pr := mail.NewPersonalization()
	pr.DynamicTemplateData = mlt.TemplateArgs
	pr.To = []*mail.Email{to}

	ml := mail.NewV3Mail()
	ml.From = from
	ml.TemplateID = mlt.Template
	ml.Personalizations = []*mail.Personalization{pr}
	ml.Subject = mlt.Subject

	resp, err := i.client.Send(ml)
	if err != nil {
		return err
	}
	if resp.StatusCode >= 400 {
		err = errors.New(resp.Body)
	}
	return err

}
func NewSendgridMailer(cnf *config.Configuration) Mailer {
	cl := sendgrid.NewSendClient(cnf.SendgridAPIKey)
	return &sendgridImpl{sender: cnf.SendgridOriginEmail, client: cl}
}
