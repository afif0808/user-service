package mailer

type (
	Mail struct {
		Subject     string
		To          string
		Body        string
		Attachments []string
	}
	MailTemplate struct {
		Subject      string
		To           string
		Template     string
		TemplateArgs map[string]interface{}
	}

	Mailer interface {
		Send(mail Mail) error
		SendWithTemplate(mlt MailTemplate) error
	}
)
