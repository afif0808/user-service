package config

import (
	cfg "gitlab.com/afif0808/golib/config"
)

type (
	Configuration struct {
		// - log config
		LogLevel     string `envconfig:"LOG_LEVEL" default:"INFO"`
		LogFilePath  string `envconfig:"LOG_FILE_PATH" default:"application.log"`
		LogFormatter string `envconfig:"LOG_FORMATTER" default:"TEXT"`
		LogMaxSize   int    `envconfig:"LOG_MAX_SIZE" default:"500"`
		LogMaxBackup int    `envconfig:"LOG_MAX_BACKUP" default:"3"`
		LogMaxAge    int    `envconfig:"LOG_MAX_BACKUP" default:"3"`
		LogCompress  bool   `envconfig:"LOG_COMPRESS" default:"true"`

		// - database
		DbUser                  string `envconfig:"DB_USER" required:"true" default:"ganymede"`
		DbPass                  string `envconfig:"DB_PASS" required:"true" default:"prixaganymede.123!"`
		DbName                  string `envconfig:"DB_NAME" required:"true" default:"ganymede"`
		DbHost                  string `envconfig:"DB_HOST" required:"true" default:"dev-mysql.prixa.private"`
		DbPort                  int    `envconfig:"DB_PORT" required:"true" default:"3306"`
		DbMaxOpenConnection     int    `envconfig:"DB_MAX_OPEN_CONNECTION" required:"true" default:"32"`
		DbMaxIdleConnection     int    `envconfig:"DB_MAX_IDLE_CONNECTION" required:"true" default:"8"`
		DbMaxLifeTimeConnection string `envconfig:"DB_MAX_LIFE_TIME_CONNECTION" required:"true" default:"1h"`
		EnableLogSqlQuery       bool   `envconfig:"LOG_SQL_QUERY" default:"false"`

		// - arango
		ArangoEndpoints         []string `envconfig:"ARANGO_ENDPOINTS" required:"true" default:"http://dev-arangodb.prixa.private:8529"`
		ArangoDatabaseName      string   `envconfig:"ARANGO_DATABASE_NAME" required:"true" default:"supertal"`
		ArangoUserName          string   `envconfig:"ARANGO_USER_NAME" required:"true" default:"test"`
		ArangoUserPass          string   `envconfig:"ARANGO_USER_PASS" required:"false" default:"t3stdb"`
		ArangoConnectionTimeout string   `envconfig:"RANGO_CONNECTION_TIMEOUT" required:"true" default:"15s"`
		ArangoMaxConnection     int      `envconfig:"ARANGO_MAX_CONNECTION" required:"true" default:"32"`

		// - voximplant
		VoximplantEndpoint      string `envconfig:"VOXIMPLANT_ENDPOINT" required:"true" default:"https://api.voximplant.com/platform_api"`
		VoximplantKeyID         string `envconfig:"VOXIMPLANT_KEY_ID" required:"true" default:"1a37694a-4b45-4eba-a85d-187df72cebd2"`
		VoximplantAccountID     string `envconfig:"VOXIMPLANT_ACCOUNT_ID" required:"true" default:"3152502"`
		VoximplantApplicationID int    `envconfig:"VOXIMPLANT_APPLICATION_ID" required:"true" default:"4349897"`
		VoximplantPrivateKey    string `envconfig:"VOXIMPLANT_KEY_PATH" required:"true" default:"-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDA6dVJiBLn4Lmv\nCJUIFx1Clx2OIrMSc1v4XAk0Ilev0KQddkSwuxMaT2EonV2e/x8uqrS3UAglhRpU\n5fvLhZXcP+eshKKZuNev2XLbjXpF2OCajf4D8Wg8/docoqWz3Nv17KyNcD+WVyLK\nmUvHc0EgoR65qNE0R40c+181KFYITp+cob3LoS7xo6sSCCMD/hSVtGlU7EiErEIo\naMFXnsr5aIfmgZwcfMtlBOgrf9ZXwkyJea1wqWz7Kj9EXn1MrTNsPllxGVkj1QiW\nuwvrdVPpvNbqy4Sfm2rVdN6BPG0VEuXVL7+5fBnW4wPGM42fz2nMu7LQak511v0c\n0fTZ3GUBAgMBAAECggEAQxlSGWheAw8UJsBabdz0TXQYUJbbKEC9rY39mYeUbk+o\ntaKsU7hN9anEhobtyF6+btHrV5Nbsgkw1nTD9s85B8mmgEgF8mfrhXuhyqHT9LVx\nsfBPePmfow88rRcZgY0BaPFwNyXiyBz4ufVF+VKqRXolILiKQ5/bgkggnnGwMuTj\nBuC6zS8gho1dKLuaMuPYFOUpINttZEANjluDvVhBI9HPq+ap5S06UkPX8M1+DC+z\nfyZVJ9IapNxovARvjVmk2AYtA7zTmW1pvzn5myqZdgFrj119JhcgRlXEJoIm+ciw\nNSQI6n0apAvx25rS/DU9AFyYECVZ28dMxOHpGtXjQQKBgQDtyawvcVe/2V80hVl5\nwLuDoYvkjL2r4Hr96dsLsa4QpdR10V1ZHAXIlCWgqTVo6JpqZYpCsTOolQMvdcnh\np4zJ+uvRcVzyNOYLm/ktRwtb4Eaw4+E8FlmZ73UQDQjMh/FcjNExIseWxk5ug8sd\nItqakcE+3j4tKUTe4/t7T+GsiQKBgQDPsE30h280nBqZUXSCn8m0G7WhS8w1CDfC\nj47//vQgECO8wZgNAdhIxs7KZL/jqWCw4oqv2P7O44S3u3m3N+hQLX1gE00jLTqr\nNsyBw58Mb6CD1otya2c7Z1RgzqBjsZmuO6o3aVKDTIW8wyLK0J8KILbz4DuJzxn6\nmMLpS/+GuQKBgH4b1+KtZNFNci2Dj0mACrKryWpjre/qtK/XB6GzENKpb/0wFSa+\nMt8GmtITC01hFwvYYDQwkGoW5UKVYIT5fDRejBRe3+FgZqS/iq4VoJOQZU1I/doF\nLG30Tgw+LW1zp3DjCcraXCm9cDkyUlVYd8FEDc3OxgDjgzAGjwf9/HypAoGABQd8\n68X30GtTTZEmZB9gcAcXv98pA8O+LezyBory+G/Lnb+GYhJAXYrK6SXFa8rnN4FU\n4Gs9yF7EEPruOJyW+JIRbikpfgeTWyAJlb7Z9CAj3+1rFQs6xbp71ZhLU/6x/QEe\n3xai0+Eff/GtvwGMSF+AbqloZ67nLyVR2vpnJqkCgYAo2tlCc/VD/2o2aTtoz+e9\n3NkQ0io0XvC0a3m+Mt3jKrwRbj1KhhfA7Nffh+hQbrhMUlZFjK8KikbiYzyjK/lZ\n56s7lpHYDsx+F+qHk96XpOa4fSw/i/s6Kn9wt/+xs4S6fYSFS2deb69cqC6+jeCO\nlou+Da7tQtBa4VuAPmYkbg==\n-----END PRIVATE KEY-----\n"`

		// - redis
		RedisHost     string `envconfig:"REDIS_HOST" required:"true" default:"localhost:6379"`
		RedisPassword string `envconfig:"REDIS_PASSWORD" required:"true" default:"password"`
		RedisDBOption int    `envconfig:"REDIS_DB_OPTION" required:"true" default:"0"`
		RedisPoolSize int    `envconfig:"REDIS_POOL_SIZE"`

		// - secret key
		SessionSecret           string `envconfig:"SESSION_SECRET" required:"true" default:"ebfWgqHlPHOrKBgUpKrz6M5A5puMfgxI"`
		RefreshSecret           string `envconfig:"REFRESH_SECRET" required:"true" default:"vFCNTEN7DCDKAOC7QwAkOXYJlncyixGM"`
		TTLSession              int64  `envconfig:"TTL_SESSION" required:"true" default:"3600"`  //in second
		TTLRefresh              int64  `envconfig:"TTL_REFRESH" required:"true" default:"86400"` //in second
		AvicennaAuthSecretKey   string `envconfig:"AVICENNA_KEY" required:"true" default:"ebfWgqHlPHOrKBgUpKrz6M5A5puMfgxI"`
		ButlerAuthSecretKey     string `envconfig:"USER_VERIFICATION_SECRET" required:"true" default:"ebfWgqHlPHOrKBgUpKrz6M5A5puMfgxI"`
		UserVerificationSecret  string `envconfig:"USER_VERIFICATION_SECRET" required:"true" default:"0JXyDD1mOL3kz9r0"`
		UserVerificationBaseURL string `envconfig:"USER_VERIFICATION_URL" required:"true" default:"http://localhost/v1"`

		// - sendgrid
		SendgridAPIKey      string `envconfig:"SENDGRID_API_KEY" default:"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"`
		SendgridOriginEmail string `envconfig:"SENDGRID_ORIGIN_EMAIL" default:"no-reply@prixa.ai"`

		// - datadog
		DatadogHost string `envconfig:"DATADOG_HOST" required:"false" default:"dev-stats-collectors.prixa.private:8125"`
		DatadogEnv  string `envconfig:"DATADOG_ENV" required:"false" default:"dev"`

		TTLRedirectToken int64 `envconfig:"TTL_REDIRECT_TOKEN" required:"true" default:"420"`
	}
)

func New() (*Configuration, error) {
	var (
		c = Configuration{}
	)

	if err := cfg.New(&c); err != nil {
		return nil, err
	}

	return &c, nil
}
