package csv

import (
	"encoding/csv"
	"errors"
	"io"
)

func ParseCSVToStruct(data io.Reader, dst []interface{}, lineLimit int) error {
	r := csv.NewReader(data)
	rows, err := r.ReadAll()
	if err != nil {
		return err
	}
	if lineLimit != 0 && len(rows) > lineLimit {
		return errors.New("rows number exceeds the limit")
	}

	for range rows {

	}
	return nil
}
