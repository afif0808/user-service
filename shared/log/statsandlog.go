package log

import (
	"fmt"
	"microservice/shared"
	"time"
)

const (
	apiKey   = "controller.api"
	apiTag   = "api:%s"
	stateTag = "state:%s"
	errTag   = "errCode:%s"
)

func StatsAndLog(sh shared.Deps, apiName string, err error, startTime time.Time, args ...interface{}) {
	if err == nil {
		sh.Logger.Info("["+apiName+"] process API successfully, args:", args)
		sh.StatsD.Duration(apiKey, startTime, fmt.Sprintf(apiTag, apiName), fmt.Sprintf(stateTag, "success"))
		return
	}

	sh.Logger.Error("["+apiName+"] failed to process API with error:", err, " args:", args)
	sh.StatsD.Duration(apiKey, startTime, fmt.Sprintf(apiTag, apiName), fmt.Sprintf(stateTag, "fail"), fmt.Sprintf(errTag, "unknown"))
}
