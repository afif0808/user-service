package shared

var Roles = map[string]bool{
	"owner":          true,
	"prx_admin":      true,
	"prx_ops":        true,
	"prx_cs":         true,
	"tele_admin":     true,
	"tele_staff":     true,
	"pharmacy_admin": true,
	"pharmacy_staff": true,
	"doctor":         true,
	"patient":        true,
}
