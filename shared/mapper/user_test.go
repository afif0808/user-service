package mapper

import (
	"microservice/entity"
	"microservice/shared/dto"
	"testing"
	"time"
)

func TestXxx(t *testing.T) {
	req := dto.UpdateUserRequest{
		Name: "Afif",
	}

	usr := entity.User{
		ID:        "1",
		Name:      "Fulan",
		Password:  "password",
		Status:    "status",
		BelongsTo: "belongsTo",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	MapUserUpdate(req, &usr)

}
