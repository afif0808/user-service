package mapper

import (
	"errors"
	"fmt"
	"microservice/entity"
	"microservice/shared/dto"
	"microservice/shared/encryptions"
	"reflect"
	"strings"
	"time"

	gonanoid "github.com/matoous/go-nanoid/v2"
	"gitlab.com/afif0808/golib/session"
)

func MapUserResponse(usr entity.User) dto.UserResponse {
	return dto.UserResponse{
		ID:        usr.ID,
		Name:      usr.Name,
		Email:     usr.Email,
		Status:    usr.Status,
		CreatedAt: usr.CreatedAt,
		UpdatedAt: usr.UpdatedAt,
		DeletedAt: usr.DeletedAt,
	}
}

func MapUserResponses(usrs []entity.User) []dto.UserResponse {
	resp := make([]dto.UserResponse, len(usrs))
	for i := range resp {
		resp[i] = MapUserResponse(usrs[i])
	}
	return resp
}

func MapUser(req dto.CreateUserRequest) (entity.User, error) {
	id, err := gonanoid.New()
	if err != nil {
		return entity.User{}, err
	}
	password, err := encryptions.EncryptPassword(req.Password)
	if err != nil {
		return entity.User{}, err
	}

	req.Password = string(password)

	req.Email = strings.ToLower(req.Email)

	rolePrefix := strings.Split(req.Role, "_")[0]

	return entity.User{
		ID:        id,
		Email:     req.Email,
		Password:  req.Password,
		Name:      req.Name,
		Status:    "pending",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		BelongsTo: rolePrefix,
		Role:      req.Role,
	}, nil
}

func MapUserUpdate(req dto.UpdateUserRequest, usr *entity.User) {
	comp := *usr
	usr.Name = req.Name
	usr.UpdatedAt = time.Now()
	fmt.Println(diff(usr, comp))
}

func MapSession(usr entity.User) session.Session {
	return session.Session{
		UserId: usr.ID,
		Email:  usr.Email,
		Name:   usr.Name,
		Role:   usr.Role,
		Iat:    time.Now().Unix(),
	}
}

func diff(x, y interface{}) ([]string, error) {
	var arr []string
	xType := reflect.TypeOf(x)
	if xType.Kind() == reflect.Ptr {
		xType = xType.Elem()
	}

	yType := reflect.TypeOf(y)
	if yType.Kind() == reflect.Ptr {
		yType = yType.Elem()
	}

	if xType.Name() != yType.Name() {
		return nil, errors.New("cannot diff of two different data")
	}

	a := reflect.ValueOf(x)
	b := reflect.ValueOf(y)

	if a.Kind() == reflect.Ptr {
		a = a.Elem()
	}

	if b.Kind() == reflect.Ptr {
		b = b.Elem()
	}

	for i := 0; i < a.NumField(); i++ {
		if a.Field(i).String() == b.Field(i).String() {
			continue
		}

		arr = append(arr, a.Type().Field(i).Name)
	}
	return arr, nil
}
