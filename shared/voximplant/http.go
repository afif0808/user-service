package voximplant

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
)

func (i *voximplantImpl) checkResponse(r *http.Response) error {
	if c := r.StatusCode; 200 <= c && c <= 299 {
		return nil
	}
	return fmt.Errorf("Request failed. Please analyze the request body for more details. Status code: %d", r.StatusCode)
}

func (i *voximplantImpl) exec(ctx context.Context, command string, body string) (*http.Response, error) {
	url := i.cfg.VoximplantEndpoint + "/" + command
	log.Println(methods[command])
	req, err := http.NewRequestWithContext(ctx, methods[command], url, strings.NewReader(body))
	if err != nil {
		return nil, err
	}
	if err != nil {
		return nil, err
	}

	token, err := i.generateToken()
	if err != nil {
		return nil, err
	}

	req.Header.Set("Authorization", "Bearer "+token)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	cl := http.Client{}
	return cl.Do(req)
}

func (i *voximplantImpl) mapHttpResp(resp *http.Response, data interface{}) error {
	err := i.checkResponse(resp)
	if err != nil {
		return err
	}
	return json.NewDecoder(resp.Body).Decode(&data)
}
