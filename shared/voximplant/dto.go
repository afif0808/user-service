package voximplant

type (
	CreateVoximplantUserParam struct {
		Name          string
		DisplayName   string
		Password      string
		ApplicationId int
		MobilePhone   string
		UserActive    bool
	}

	UpdateVoximplantUserParam struct {
		ID            string
		DisplayName   string
		MobilePhone   string
		UserActive    bool
		ApplicationId int
	}

	CreateVoximplantUserResponse struct {
		VoximplantUserID int `json:"user_id"`
	}
)
