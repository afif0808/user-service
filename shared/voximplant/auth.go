package voximplant

import (
	"time"

	"github.com/golang-jwt/jwt"
)

func (i *voximplantImpl) generateToken() (string, error) {
	jwtNewWithClaims := jwt.NewWithClaims(jwt.GetSigningMethod("RS256"), jwt.MapClaims{
		"iat": time.Now().Unix(),
		// "iss": kp.Result.AccountId,
		"iss": i.cfg.VoximplantAccountID,
		"exp": time.Now().Add(time.Second * tokenDuration).Unix(),
	})
	jwtNewWithClaims.Header["kid"] = i.cfg.VoximplantKeyID

	privateKey, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(i.cfg.VoximplantPrivateKey))
	if err != nil {
		return "", err
	}
	return jwtNewWithClaims.SignedString(privateKey)
}
