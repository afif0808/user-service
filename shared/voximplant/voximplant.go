package voximplant

import (
	"context"
	"log"
	"microservice/shared/config"
	"net/url"
	"strconv"
)

type (
	voximplantImpl struct {
		cfg *config.Configuration
	}

	Voximplant interface {
		CreateVoximplantUser(ctx context.Context, param CreateVoximplantUserParam) (CreateVoximplantUserResponse, error)
		UpdateVoximplantUser(ctx context.Context, p UpdateVoximplantUserParam) error
	}
)

const (
	tokenDuration = 30
)

var (
	methods = map[string]string{
		"AddUser":     "POST",
		"SetUserInfo": "POST",
	}
)

func (i *voximplantImpl) CreateVoximplantUser(ctx context.Context, p CreateVoximplantUserParam) (CreateVoximplantUserResponse, error) {
	payload := url.Values{}
	payload.Add("application_id", strconv.Itoa(i.cfg.VoximplantApplicationID))
	payload.Add("user_display_name", p.DisplayName)
	payload.Add("user_name", p.Name)
	payload.Add("user_password", p.Password)
	payload.Add("user_active", strconv.FormatBool(p.UserActive))
	log.Println(payload.Encode())

	httpResp, err := i.exec(ctx, "AddUser", payload.Encode())
	if err != nil {
		return CreateVoximplantUserResponse{}, err
	}

	var resp CreateVoximplantUserResponse
	err = i.mapHttpResp(httpResp, &resp)
	if err != nil {
		log.Println("disini", err)
		return CreateVoximplantUserResponse{}, err
	}
	return resp, nil
}

func (i *voximplantImpl) UpdateVoximplantUser(ctx context.Context, p UpdateVoximplantUserParam) error {
	payload := url.Values{}
	payload.Add("application_id", strconv.Itoa(i.cfg.VoximplantApplicationID))
	payload.Add("user_display_name", p.DisplayName)
	payload.Add("user_active", strconv.FormatBool(p.UserActive))
	log.Println(payload.Encode())

	_, err := i.exec(ctx, "SetUserInfo", payload.Encode())
	return err

}

func NewVoximplant(cfg *config.Configuration) Voximplant {

	return &voximplantImpl{cfg: cfg}

}
