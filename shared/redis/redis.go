package redis

import (
	"context"
	"fmt"
	"microservice/shared/config"

	"github.com/go-redis/redis/v8"
)

type (
	RedisClient interface {
		Get(ctx context.Context, key string) (interface{}, error)
		Set(ctx context.Context, key string, value interface{}, duration int64) error
		Delete(ctx context.Context, key string) error
	}

	redisInstance struct {
		client *redis.Client
		conf   *config.Configuration
	}
)

func (inst *redisInstance) Get(ctx context.Context, key string) (interface{}, error) {
	return inst.client.Do(ctx, "get", key).Result()
}

func (inst *redisInstance) Set(ctx context.Context, key string, val interface{}, duration int64) error {
	_, err := inst.client.Do(ctx, "set", key, val, "EX", duration).Result()
	return err
}

func (inst *redisInstance) Delete(ctx context.Context, key string) error {
	_, err := inst.client.Do(ctx, "del", key).Result()
	return err
}

func NewRedisClient(conf *config.Configuration) RedisClient {

	opt := &redis.Options{
		Network:  "tcp",
		Addr:     conf.RedisHost,
		DB:       conf.RedisDBOption,
		Password: conf.RedisPassword,
	}
	fmt.Println("redis_password", conf.RedisPassword)
	client := redis.NewClient(opt)
	return &redisInstance{client: client, conf: conf}
}
