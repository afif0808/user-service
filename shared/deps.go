package shared

import (
	"gitlab.com/afif0808/golib/db"
	"gitlab.com/afif0808/golib/logger"
	"gitlab.com/afif0808/golib/telemetry/prixastats"
	"go.uber.org/dig"

	"microservice/shared/config"
)

type (
	Deps struct {
		dig.In

		Config          *config.Configuration
		Logger          logger.Logger
		ORM             db.ORM
		ODM             db.ODM
		CustomValidator *CustomValidator
		StatsD          *prixastats.Client
	}
)

func Register(container *dig.Container) error {

	if err := container.Provide(NewCustomValidator); err != nil {
		return err
	}

	return nil
}
