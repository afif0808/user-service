package shared

import (
	"github.com/go-playground/validator/v10"
)

type (
	CustomValidator struct {
		validator *validator.Validate
	}
)

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)

}

func NewCustomValidator() (*CustomValidator, error) {
	cv := &CustomValidator{validator: validator.New()}

	err := cv.validator.RegisterValidation("role", vaidateRole)

	return cv, err
}

func vaidateRole(fl validator.FieldLevel) bool {
	return Roles[fl.Field().String()]
}
