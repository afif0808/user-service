package auth

import (
	"fmt"
	"microservice/shared/encryptions"
	"net/http"
	"strconv"
	"time"
)

func ApplyHMACAuthToRequest(req *http.Request, secretKey, source string) {
	timestamp := time.Now().Unix()
	payload := fmt.Sprintf("%s:%s:%s:%d", req.Method, req.URL.RequestURI(), source, timestamp)
	signature := encryptions.HMACSHA256Hex(secretKey, []byte(payload))
	fmt.Println(payload)
	req.Header.Set("x-source", source)
	req.Header.Set("x-timestamp", strconv.FormatInt(timestamp, 10))
	req.Header.Set("x-signature", signature)
}
