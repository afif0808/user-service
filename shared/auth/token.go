package auth

import (
	"microservice/shared/config"
	"microservice/shared/dto"
	"time"

	"gitlab.com/afif0808/golib/crypto"
	"gitlab.com/afif0808/golib/session"
)

func GenerateAuthToken(ss session.Session, cnf *config.Configuration) (dto.AuthTokenPayload, error) {
	var resp dto.AuthTokenPayload
	now := time.Now().Unix()

	accessTokenExpiredTime := now + cnf.TTLSession
	refreshTokenExpiredTime := now + cnf.TTLRefresh
	sessionCrypt, err := crypto.New(cnf.SessionSecret)
	if err != nil {
		return dto.AuthTokenPayload{}, err
	}
	ss.Expired = accessTokenExpiredTime
	resp.AccessToken, err = ss.Encrypt(sessionCrypt)
	if err != nil {
		return dto.AuthTokenPayload{}, err
	}

	ss.Expired = refreshTokenExpiredTime
	refreshTokenCrypt, err := crypto.New(cnf.RefreshSecret)
	if err != nil {
		return dto.AuthTokenPayload{}, err
	}
	resp.RefreshToken, err = ss.Encrypt(refreshTokenCrypt)
	if err != nil {
		return dto.AuthTokenPayload{}, err
	}
	return resp, nil
}
