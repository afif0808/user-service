package encryptions

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"

	"golang.org/x/crypto/bcrypt"
)

func HMACSHA256Hex(secretKey string, payload []byte) string {
	hash := hmac.New(sha256.New, []byte(secretKey))
	hash.Write([]byte(payload))
	return hex.EncodeToString(hash.Sum(nil))
}

func EncryptPassword(password string) (string, error) {
	b, err := bcrypt.GenerateFromPassword([]byte(password), 12)
	if err != nil {
		return "", err
	}
	return string(b), nil
}
