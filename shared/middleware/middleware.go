package middleware

import (
	"fmt"
	"microservice/shared"
	"microservice/shared/encryptions"
	"microservice/shared/log"
	"strconv"
	"strings"
	"time"

	"gitlab.com/afif0808/golib/crypto"
	"gitlab.com/afif0808/golib/echo/middlewares"

	"github.com/casbin/casbin/v2"
	"github.com/labstack/echo/v4"
	"gitlab.com/afif0808/golib/context"
	"gitlab.com/afif0808/golib/errors"
	"gitlab.com/afif0808/golib/logger"
)

type (
	Middlewares interface {
		CheckPermission(next echo.HandlerFunc) echo.HandlerFunc
		Authenticate(next echo.HandlerFunc) echo.HandlerFunc
		StatsAndLog(apiName string) echo.MiddlewareFunc
	}

	middlewaresInst struct {
		enforcer *casbin.Enforcer
		deps     shared.Deps
		ssmw     middlewares.SessionMiddleware
	}
)

func (mw *middlewaresInst) CheckPermission(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		if bypassed, _ := ctx.Get("BYPASSED").(bool); bypassed {
			return next(ctx)
		}
		pctx, ok := ctx.(*context.PrixaContext)
		if !ok {
			return errors.ErrSession
		}

		object := ctx.Request().URL.String()
		action := strings.ToLower(ctx.Request().Method)
		domain := "general"

		if self, _ := strconv.ParseBool(ctx.QueryParam("self")); self {
			domain = "self"
		}
		hasPermission, err := mw.enforcer.Enforce(pctx.Session.Role, domain, object, action)
		if err != nil {
			return err
		}

		if !hasPermission {
			return errors.ErrSession
		}

		return next(ctx)
	}
}

func (mw *middlewaresInst) Authenticate(next echo.HandlerFunc) echo.HandlerFunc {
	secretKeys := map[string]string{
		"avicenna": mw.deps.Config.AvicennaAuthSecretKey,
		"butler":   mw.deps.Config.ButlerAuthSecretKey,
	}

	return func(ctx echo.Context) error {
		source := ctx.Request().Header.Get("x-source")
		signature := ctx.Request().Header.Get("x-signature")
		timestamp, _ := strconv.ParseInt(ctx.Request().Header.Get("x-timestamp"), 10, 64)

		if signature == "" {
			return mw.ssmw.AuthenticateSession(next)(ctx)
		}

		secretKey, ok := secretKeys[source]
		if !ok {
			return shared.AuthPrixaRequestInvalidSourceHeaderError()
		}
		if diff := timestamp - time.Now().Unix(); diff < 0 || diff > 300 {
			return shared.AuthPrixaRequestExpiredError()
		}
		payload := fmt.Sprintf("%s:%s:%s:%d", ctx.Request().Method, ctx.Request().RequestURI, source, timestamp)
		validSignature := encryptions.HMACSHA256Hex(secretKey, []byte(payload))
		if validSignature != signature {
			return shared.AuthPrixaRequestInvalidSignatureError()
		}

		ctx.Set("BYPASSED", true)

		return next(ctx)
	}
}

func (mw *middlewaresInst) StatsAndLog(apiName string) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) error {
			t := time.Now()
			err := next(ctx)
			log.StatsAndLog(mw.deps, apiName, err, t)
			return err
		}
	}
}

func NewMiddlewares(deps shared.Deps, crypto crypto.Crypto, logger logger.Logger) Middlewares {
	enf, err := casbin.NewEnforcer("./keymatch_model.conf", "./keymatch_policy.csv")
	if err != nil {
		panic(err)
	}

	ssmw, err := middlewares.NewSessionMiddleware(deps.Config.SessionSecret, crypto, logger)
	if err != nil {
		panic(err)
	}

	return &middlewaresInst{enforcer: enf, ssmw: ssmw, deps: deps}
}
