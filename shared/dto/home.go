package dto

import "gitlab.com/afif0808/golib/session"

type HomeResponse struct {
	Message string `json:"message"`
}

type ValidateTokenRequest struct {
	Token string `json:"token"`
}

type ValidateTokenResponse struct {
	*session.Session
}
