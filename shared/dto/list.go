package dto

type ListInfo struct {
	IsFirstPage bool  `json:"isFirstPage"`
	IsLastPage  bool  `json:"isLastPage"`
	Page        int64 `json:"page"`
	Limit       int64 `json:"limit"`
	RecordCount int64 `json:"recordCount"`
	PageCount   int64 `json:"pageCount"`
}
