package dto

import (
	"time"
)

type CreateUserRequest struct {
	Email       string  `json:"email" validate:"required"`
	Password    string  `json:"password" validate:"required"`
	Name        string  `json:"name" validate:"required"`
	Role        string  `json:"-"`
	ClientAppID *string `json:"-"`
	ClientID    *string `json:"-"`
	CreatedFrom string  `json:"-"`
}

type UpdateUserRequest struct {
	Name string `json:"name" validate:"required"`
}

type ResendVerificationRequest struct {
	Email      string `json:"email" validate:"required"`
	RolePrefix string `json:"rolePrefix" validate:"required"`
}

type UpdateUserStatusRequest struct {
	ID     string
	Status string
	Role   string
}

type UpdateUserPasswordRequest struct {
	ID              string `json:"id" validate:"required"`
	CurrentPassword string `json:"currentPassword" validate:"required"`
	NewPassword     string `json:"newPassword" validate:"required"`
}
type UpdateUserStatusAndPasswordRequest struct {
	ID       string
	Password string
	Role     string
	Status   string
}

type UserResponse struct {
	ID          string     `json:"id"`
	Email       string     `json:"email"`
	Name        string     `json:"name"`
	Status      string     `json:"status"`
	CreatedAt   time.Time  `json:"createdAt"`
	UpdatedAt   time.Time  `json:"updatedAt"`
	DeletedAt   *time.Time `json:"deletedAt,omitempty"`
	ClientID    *string    `json:"clientId,omitempty"`
	ClientAppID *string    `json:"clientAppId,omitempty"`
}

type UserLoginRequest struct {
	Email      string `json:"email" validate:"required"` // it could be email , username , etc..
	Password   string `json:"password" validate:"required"`
	RolePrefix string `json:"rolePrefix" validate:"required"`
}

type AuthTokenPayload struct {
	AccessToken  string `json:"accessToken"`
	RefreshToken string `json:"refreshToken"`
}

type UserLoginResponse struct {
	AuthTokenPayload
}

// type UserVerificationPayload struct {
// 	UserID   string `json:"user_id"`
// 	UserRole string `json:"user_role" validate:"required"`
// 	jwt.StandardClaims
// }

type UserRefreshTokenRequest struct {
	RefreshToken string `json:"refreshToken" validate:"required"`
}
type UserRefreshTokenResponse struct {
	AuthTokenPayload
}

type ResetPasswordRequest struct {
	NewPassword string `json:"newPassword" validate:"required"`
	Token       string `json:"token" validate:"required"`
}

type PasswordResetRequestParam struct {
	Email      string `json:"email" validate:"required"`
	RolePrefix string `json:"role" validate:"required"`
}

type CreateRedirectTokenRequest struct {
	ClientAppID string `json:"clientAppId" validate:"required"`
	UserID      string `json:"-" validate:"required"`
}
type CreateRedirectTokenResponse struct {
	Token string `json:"token"`
}

type ClaimRedirectTokenRequest struct {
	Token string `json:"token" validate:"required"`
}

type VerifyUserRequest struct {
	Token       string  `json:"token" validate:"required"`
	NewPassword *string `json:"newPassword,omitempty"`
}
