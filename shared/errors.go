package shared

import (
	"net/http"

	"gitlab.com/afif0808/golib/errors"
)

func BadRequestError(err error) error {
	return errors.ErrBase.New(err.Error()).
		WithProperty(errors.ErrCodeProperty, http.StatusBadRequest).
		WithProperty(errors.ErrHttpCodeProperty, http.StatusBadRequest)
}

func UnprocessableEntityError(err error) error {
	return errors.ErrBase.New(err.Error()).
		WithProperty(errors.ErrCodeProperty, http.StatusUnprocessableEntity).
		WithProperty(errors.ErrHttpCodeProperty, http.StatusUnprocessableEntity)
}

func UnauthorizedError(err error) error {
	return errors.ErrBase.New(err.Error()).
		WithProperty(errors.ErrCodeProperty, http.StatusUnauthorized).
		WithProperty(errors.ErrHttpCodeProperty, http.StatusUnauthorized)
}

func NotFoundError(err error) error {
	return errors.ErrBase.New(err.Error()).
		WithProperty(errors.ErrCodeProperty, http.StatusNotFound).
		WithProperty(errors.ErrHttpCodeProperty, http.StatusNotFound)
}

func ConflictError(err error) error {
	return errors.ErrBase.New(err.Error()).
		WithProperty(errors.ErrCodeProperty, http.StatusConflict).
		WithProperty(errors.ErrHttpCodeProperty, http.StatusConflict)
}

//ErrAuthPrixaRequestInvalidSourceHeader is
func AuthPrixaRequestInvalidSourceHeaderError() error {
	return errors.ErrBase.New("invalid header x-source").WithProperty(errors.ErrCodeProperty, 2201).WithProperty(errors.ErrHttpCodeProperty, http.StatusUnauthorized)
}

//ErrAuthPrixaRequestInvalidTimestampHeader is
func AuthPrixaRequestInvalidTimestampHeaderError() error {
	return errors.ErrBase.New("invalid header x-timestamp").WithProperty(errors.ErrCodeProperty, 2202).WithProperty(errors.ErrHttpCodeProperty, http.StatusUnauthorized)
}

//ErrAuthPrixaRequestExpired is
func AuthPrixaRequestExpiredError() error {
	return errors.ErrBase.New("request is expired").WithProperty(errors.ErrCodeProperty, 2203).WithProperty(errors.ErrHttpCodeProperty, http.StatusUnauthorized)
}

//ErrAuthPrixaRequestInvalidSignature is
func AuthPrixaRequestInvalidSignatureError() error {
	return errors.ErrBase.New("invalid signature").WithProperty(errors.ErrCodeProperty, 2203).WithProperty(errors.ErrHttpCodeProperty, http.StatusUnauthorized)
}
